# Loop

```
Get-ChildItem "C:\Users\{usr}\Downloads" -Filter apache*.zip |
Foreach-Object {
    $filename = $_.Name
    Write-Host "$filename"
}
```

# File parsing

Trim empty lines
Find required string

```ps
(Get-Content txt.txt) -notmatch '^\s*$' | Select-String -Pattern 'Description' -Context 0,1 | ForEach-Object { $description = $_.Context.PostContext }
Write-Host "$description"
```
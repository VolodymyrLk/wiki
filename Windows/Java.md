# Install JDK

https://docs.oracle.com/javase/10/install/installation-jdk-and-jre-microsoft-windows-platforms.htm

Installation Scenario
Install JDK and public JRE in silent mode.	
jdk.exe /s
Install development tools and source code in silent mode but not the public JRE.	
jdk.exe /s ADDLOCAL="ToolsFeature,SourceFeature"
Install development tools, source code, and the public JRE in silent mode.	
jdk.exe /s ADDLOCAL="ToolsFeature,SourceFeature,PublicjreFeature"
Install the public JRE in the specified directory C:\test in silent mode.	
jdk.exe /s /INSTALLDIRPUBJRE=C:\test


jdk-12.0.1_windows-x64_bin.exe /s ADDLOCAL="ToolsFeature,SourceFeature" INSTALLDIR="C:\java\jdk-12.0.1"

Link to installation using PowerShell:
[a relative link](jdk_install.ps1)
[a relative link](jdk_download_install.ps1)
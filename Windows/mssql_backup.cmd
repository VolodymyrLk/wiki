@echo off
rem Get current time
For /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set mydate=%%c-%%a-%%b)
For /f "tokens=1-2 delims=/:" %%a in ("%TIME%") do (set mytime=%%a%%b)
echo %mydate%_%mytime%

rem Proceed with backup
echo Creating backup %~5\%~4_%mydate%_%mytime%.bak for database %~4 on server %~3

SqlCmd -U %~1 -P %~2 -S %~3 -Q "BACKUP DATABASE %~4 TO DISK='%~5\%~4_%mydate%_%mytime%.bak'"
﻿if (!(Test-Path -Path "C:\java\jdk-12.0.1\bin\java.exe"))
{
    if (!(Test-Path -Path "C:\app_sources"))
    {
    New-Item -path "C:\app_sources" -itemtype directory
    }
    Remove-Item -Path "C:\java\*" -Recurse -Verbose -ErrorAction SilentlyContinue
    Invoke-WebRequest -Uri "https://java.source/jdk-12.0.1_windows-x64_bin.exe" -OutFile "C:\app_sources\jdk-12.0.1_windows-x64_bin.exe"
    if (!(Test-Path -Path "C:\java"))
    {
    New-Item -path "C:\java"
    }
    $arguments = "/s ADDLOCAL=`"ToolsFeature,SourceFeature`" INSTALLDIR=`"C:\java\jdk-12.0.1`""
    $proc = Start-Process -FilePath "jdk-12.0.1_windows-x64_bin.exe" -ArgumentList $arguments -Wait -PassThru
    if($proc.ExitCode -ne 0)
    {
    Throw "Could not install JDK"
    }
    Remove-Item -Path "C:\app_sources\*" -Recurse -Verbose -ErrorAction SilentlyContinue
}
# Silennt Install

http://www.get-itsolutions.com/silent-install-google-chrome-and-disable-auto-update/

@echo install Google Chrome

start /wait msiexec /i C:\Users\{usr}\Downloads\GoogleChromeEnterpriseBundle64\GoogleChromeStandaloneEnterprise64.msi /quiet /passive

@echo Set the parameter file


# Check if Chrome installed and set Chrome path

for /f "usebackq tokens=1,2,3,4,5" %a in (`reg query HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\ /s /f \chrome.exe ^| findstr Application`) do set CHROMEPATH=%c%d%e

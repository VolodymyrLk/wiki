$software = "Google Chrome";
$installed = (Get-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\* | Where { $_.DisplayName -Match $software }) -ne $null

If(-Not $installed) {
	Write-Host "'$software' is NOT installed."
    $arguments = "/i `"GoogleChromeStandaloneEnterprise64.msi`" /q /norestart"
    $proc = Start-Process -FilePath "msiexec.exe" -ArgumentList $arguments -Wait -PassThru
    if($proc.ExitCode -ne 0)
    {
    Throw "Could not install Chrome"
    }
} else {
	Write-Host "'$software' is installed."
}
# Installation and setup

> Download binary

http://apache.ip-connect.vn.ua/maven/maven-3/3.6.1/binaries/apache-maven-3.6.1-bin.zip

> General

https://docs.wso2.com/display/IS323/Installing+Apache+Maven+on+Windows

mvn -f .\pom.xml clean verify

> Required Envrironmet variables, e.g.:

```JAVA_HOME```
C:\Program Files\Java\jdk-12.0.1

```M2_HOME```
C:\maven\apache-maven-3.6.1

```Path``` adjusting
C:\maven\apache-maven-3.6.1\bin

Check if everything were setup correctly
mvn --version
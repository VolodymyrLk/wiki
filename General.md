# Java Download

> General

https://www.oracle.com/downloads/index.html

# JDK

https://www.oracle.com/technetwork/java/javase/downloads/index.html

> Java 12 Documentation

https://docs.oracle.com/en/java/javase/12/

# JRE

https://www.java.com/en/download/

# Java Home

> Installing the JDK Software and Setting JAVA_HOME

https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/

# Zip files using JDK

https://www.geeksforgeeks.org/jar-files-java/
https://download.oracle.com/otn_hosted_doc/jdeveloper/904preview/jdk14doc/docs/tooldocs/windows/jar.html
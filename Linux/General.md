# Linux version

1. uname -a – kernel version
2. rpm --query centos-release – for CentOS
3. hostnamectl – for Ubuntu
4. lsb_release -d – for Ubuntu

# Services list

1. systemctl list-unit-files --type=service
2. ps -faux – processes tree
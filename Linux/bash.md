# Run bash over the loop with values that have spaces in items

```
custom_tags="Some Project,Application,Owner,Purpose"
IFS="," read -a tags <<< $custom_tags

for (( i=0; i<${#tags[@]}; i++ )); do
    subtype=${tags[$i]}
    echo "Subtype: $subtype"
done
```
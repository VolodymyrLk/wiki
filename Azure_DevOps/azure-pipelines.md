# Azure Pipelines

**.Net app build and deploy at Azure**

[Basic tasks regarding build .Net app and Deploy at Azure Web App](azure-pipelines-dot-net.yml)
Pay attention:
```PipelineBuildConfiguration```=```dev``` as a queue variable

---

**Run Maven**

[Run Maven](azure-pipelines-maven.yml)

---

**General tasks**

[General tasks](azure-pipelines-other.yml)

---
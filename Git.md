# Git commands

1.	git fetch -p (or git fetch --prune) – [refresh the list of remote branches](https://stackoverflow.com/questions/45191234/how-can-i-refresh-the-list-of-remote-branches-in-my-visual-studio-2017-team-expl)
2.	git ls-remote – list of remote branches
3.	git checkout -q develop – change local branch
4.	git reset HEAD~ – revert commit which not pushed
5.	Create new branch
    - git checkout -b your_branch – [first, you must create your branch locally](https://stackoverflow.com/questions/1519006/how-do-you-create-a-remote-git-branch)
    - git push -u origin your_branch